import sys
from netmiko import ConnectHandler

def script(router, user, password):
    try:
        # r = {'device_type': 'cisco_ios', 'ip': router, 'username': 'test', 'password': 'test' }
        r = {'device_type': 'cisco_ios', 'ip': router, 'username': user, 'password': password }
        ssh = ConnectHandler(**r)
        x = ssh.send_command('sh ip int br')
        print(x)
    except Exception as err:
        exception_type = type(err).__name__
        print(f""" - - - - - - - - -    {exception_type}    - - - - - - - - -""")


r = sys.argv
router = r[1]
user = r[2]
password = r[3]

# router = '192.168.183.168'
script(router, user, password)